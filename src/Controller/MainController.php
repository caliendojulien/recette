<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MainController extends AbstractController
{
    #[Route("/", name: "home")]
    public function home(
        HttpClientInterface $api
    ): Response
    {
        $reponseApi = $api->request( // Je fais la requete
            'GET',
            'https://pokeapi.co/api/v2/pokemon/pikachu'
        );
        $pikachu = $reponseApi->toArray(); // Je recupere le contenu
        return $this->render(
            'main/home.html.twig',
            compact('pikachu') // Je l'envoie a mon twig
        );
    }
}
