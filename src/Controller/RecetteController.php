<?php

namespace App\Controller;

use App\Entity\Recette;
use App\Repository\RecetteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/recette", name: "recette_")]
class RecetteController extends AbstractController
{
    #[Route("liste", name: "liste")]
    public function liste(
        RecetteRepository $repo,
        Request           $request
    ): Response
    {
        $tri = $request->cookies->get('tri');
        if ($tri === "favori") {
            $liste = $repo->findBy([], ["est_favori" => "DESC"]);
        } elseif ($tri === "nom") {
            $liste = $repo->findBy([], ['nom' => 'ASC']);
        } else {
            $liste = $repo->findAll();
        }
        return $this->render('recette/liste.html.twig',
            compact("liste")
        );
    }

    #[Route("/modifiefavori/{id}", name: "modifie_favori")]
    public function modifiefavori(
        int                    $id,
        RecetteRepository      $recetteRepository,
        EntityManagerInterface $em): RedirectResponse
    {
        $recette = $recetteRepository->find($id); // Je récupère la recette concernée
        $recette->setEstFavori(!$recette->getEstFavori()); // Je lui mets l'inverse de ce qu'elle est
        $em->persist($recette); // Je persist
        $em->flush(); // Je flush
        $this->addFlash('success', $recette->getNom() . ' a été ' . $recette->getEstFavori() ? 'ajoutée' : 'retirée' . ' des favoris');
        return $this->redirectToRoute('recette_liste'); // Je redirige vers la liste
    }

    #[Route("/details/{id}", name: "details")]
    public function details(Recette $recette): Response
    {
        return $this->render('recette/details.html.twig',
            compact('recette')
        );
    }

    #[Route("/tri/{param}", name: "tri")]
    public function tri(
        string            $param,
        RecetteRepository $repo,
    ): Response
    {
        if ($param == 'favori') {
            $liste = $repo->findBy([], ["est_favori" => "DESC"]);
//            $cookie = new Cookie('tri', 'favori', strtotime("+1 year"));
        } else {
            $liste = $repo->findBy([], ["nom" => "ASC"]);
//            $cookie = new Cookie('tri', 'nom', strtotime("+1 year"));
        }
//        $response = new Response();
//        $response->headers->setCookie($cookie);
//        $response->send();
        return $this->render('recette/liste.html.twig',
            compact("liste")
        );
    }

}
